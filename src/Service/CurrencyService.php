<?php
namespace App\Service;

use App\Repository\CurrencyRepository;

class CurrencyService {

    protected $currencyRepository;

    public function __construct(CurrencyRepository $currencyRepository){
        $this->currencyRepository = $currencyRepository;
    }

    public function getCurrencyList(){
        return  $this->currencyRepository->findAll();
    }

}