<?php
namespace App\Service;

use App\Repository\CountryRepository;
use Symfony\Component\HttpFoundation\RequestStack;


class CountryService {

    protected $countryRepository;
    protected $requestStack;

    public function __construct(CountryRepository $countryRepository, RequestStack $requestStack){
        $this-> countryRepository = $countryRepository;
        $this->requestStack = $requestStack;
    }

    public function getCountryList(){
        return  $this->countryRepository->findAll();
    }

    public function getSelectCountry(){
        $request = $this->requestStack->getCurrentRequest();
        //dd($request->getLocale());
        return $this->countryRepository->findOneBy(['countrycodelang'=> $request->getLocale()]);
    }

}